
import 'package:app_test_employee_manager/login_check.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      localizationsDelegates: [
        GlobalWidgetsLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      supportedLocales: [Locale('en', 'US')],
      theme: new ThemeData(
        scaffoldBackgroundColor: const Color.fromARGB(255, 255, 210, 28),
        fontFamily: 'GmarketSans',
        primarySwatch: Colors.brown,
      ),
      home: const LoginCheck(),
    );
  }
}