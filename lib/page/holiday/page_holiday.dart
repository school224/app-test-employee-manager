import 'package:app_test_employee_manager/components/component_custom_loading.dart';
import 'package:app_test_employee_manager/components/component_notification.dart';
import 'package:app_test_employee_manager/config/config_color.dart';
import 'package:app_test_employee_manager/config/config_decoration.dart';
import 'package:app_test_employee_manager/config/config_form_decoration.dart';
import 'package:app_test_employee_manager/config/config_form_validator.dart';
import 'package:app_test_employee_manager/config/config_size.dart';
import 'package:app_test_employee_manager/model/holiday/holiday_create_request.dart';
import 'package:app_test_employee_manager/model/holiday/holiday_history_item.dart';
import 'package:app_test_employee_manager/repository/repo_holiday.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageHoliday extends StatefulWidget {
  const PageHoliday({Key? key}) : super(key: key);

  @override
  State<PageHoliday> createState() => _PageHolidayState();
}

class _PageHolidayState extends State<PageHoliday> {
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();

  Map<DateTime, List<NeatCleanCalendarEvent>> _events = {};

  late DateTime _selectedDay;
  late DateTime _today;

  bool _isViewDetail = false;


  Map<DateTime, List<NeatCleanCalendarEvent>> _convertDayEvents(
      List<HolidayHistoryItem>? items) {
    Map<DateTime, List<NeatCleanCalendarEvent>> result = {};

    for (HolidayHistoryItem item in items!) {
      List<NeatCleanCalendarEvent> inList = [];
      inList.add(NeatCleanCalendarEvent(item.holidayType,
          description: item.reason,
          isAllDay: true,
          startTime: item.dateDesired,
          endTime: item.dateDesired));

      result.putIfAbsent(item.dateDesired, () => inList);
    }

    return result;
  }

  Future<void> _loadItems() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoHoliday()
        .getList()
        .then((res) => {
      BotToast.closeAllLoading(),
      setState(() {
        _events = _convertDayEvents(res.list);
      })
    })
        .catchError((err) => {
      BotToast.closeAllLoading(),
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _today = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
      _selectedDay = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
    });
    _loadItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Calendar(
          onDateSelected: _handleSelectedDate,
          onMonthChanged: _handleMonthChange,
          eventListBuilder: _eventListBuilder,
          hideTodayIcon: true,
          startOnMonday: true,
          weekDays: ['월', '화', '수', '목', '금', '토', '일'],
          events: _events,
          isExpandable: false,
          isExpanded: true,
          eventDoneColor: Colors.green,
          selectedColor: Colors.pink,
          todayColor: Colors.blue,
          eventColor: Colors.grey,
          locale: 'ko_KR',
          expandableDateFormat: 'yyyy년 MM월 dd일 EEEE',
          dayOfWeekStyle: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w800, fontSize: 11),
        ),
      ),
    );
  }

  Widget _eventListBuilder(BuildContext context, List<NeatCleanCalendarEvent> eventList) {
    if (_isViewDetail) {
      if (eventList.length <= 0) {
        if (_selectedDay.isAfter(_today) || _selectedDay.isAtSameMomentAs(_today)) {
          return Expanded(
            child: ListView(
              children: [
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        padding: const EdgeInsets.only(
                          top: 10,
                          bottom: 10,
                          left: 20,
                          right: 20,
                        ),
                        child: FormBuilder(
                          key: _formKey,
                          autovalidateMode: AutovalidateMode.disabled,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                decoration: formBoxDecoration,
                                padding: bodyPaddingAll,
                                child: FormBuilderDropdown(
                                  name: 'holidayKind',
                                  decoration: ConfigFormDecoration()
                                      .getInputDecoration('휴무타입'),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(
                                        errorText: formErrorRequired),
                                  ]),
                                  items: const [
                                    DropdownMenuItem(
                                      value: 'AM',
                                      child: Text('오전'),
                                    ),
                                    DropdownMenuItem(
                                      value: 'PM',
                                      child: Text('오후'),
                                    ),
                                    DropdownMenuItem(
                                      value: 'DAY',
                                      child: Text('전일'),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                decoration: formBoxDecoration,
                                padding: bodyPaddingAll,
                                child: FormBuilderTextField(
                                  name: 'reason',
                                  decoration: ConfigFormDecoration()
                                      .getInputDecoration('사유'),
                                  maxLength: 30,
                                  keyboardType: TextInputType.text,
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(
                                        errorText: formErrorRequired),
                                    FormBuilderValidators.minLength(2,
                                        errorText: formErrorMinLength(2)),
                                    FormBuilderValidators.maxLength(30,
                                        errorText: formErrorMaxLength(30)),
                                  ]),
                                ),
                              ),
                              Container(
                                child: OutlinedButton(
                                  child: Text('등록'),
                                  onPressed: () {
                                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                                      String formattedDate = DateFormat('yyyy-MM-dd').format(_selectedDay);
                                      HolidayCreateRequest createRequest =
                                      HolidayCreateRequest(
                                        _formKey.currentState!.fields['holidayType']!.value,
                                        formattedDate,
                                        _formKey.currentState!.fields['reason']!.value,
                                      );
                                      _showCreateDialog(createRequest);
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        } else {
          return Container();
        }
      } else {
        return Expanded(
          child: ListView.builder(
            padding: EdgeInsets.all(0.0),
            itemBuilder: (BuildContext context, int index) {
              final NeatCleanCalendarEvent event = eventList[index];
              return ListTile(
                contentPadding:
                EdgeInsets.only(left: 2.0, right: 8.0, top: 2.0, bottom: 2.0),
                leading: Container(
                  width: 10.0,
                  color: event.color,
                ),
                title: Text(
                  event.summary,
                  style: TextStyle(height: 1.5, fontSize: fontSizeMid),
                ),
                subtitle: Text(
                  event.description,
                  style: TextStyle(
                      height: 1.3, color: colorGray, fontSize: fontSizeSm),
                ),
                trailing: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [isShowDelButton(event.startTime)],
                ),
              );
            },
            itemCount: eventList.length,
          ),
        );
      }
    } else {
      return Container();
    }

  }

  Widget isShowDelButton(DateTime dateHoliday) {
    String formattedDate = DateFormat('yyyy-MM-dd').format(dateHoliday);

    if (dateHoliday.isAfter(_today)) {
      return IconButton(
        icon: Icon(Icons.dangerous),
        color: colorPrimary,
        onPressed: () {
          _showDelDialog(formattedDate);
        },
      );
    } else {
      return IconButton(
        icon: Icon(Icons.dangerous),
        color: colorLightGray,
        onPressed: () {},
      );
    }
  }

  Future<void> _doSetHoliday(BuildContext context, HolidayCreateRequest holidayCreateRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoHoliday().setData(holidayCreateRequest).then((res) {
      BotToast.closeAllLoading();
      _loadItems();
      ComponentNotification(
        success: true,
        title: '휴무 등록 성공',
        subTitle: '휴무 등록이 성공하였습니다.',
      ).call();
      setState(() {
        _isViewDetail = false;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '휴무 등록 실패',
        subTitle: '관리자에게 문의하세요.',
      ).call();
    });
  }

  Future<void> _doDelHoliday(BuildContext context, String dateHoliday) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoHoliday().delData(dateHoliday).then((res) {
      BotToast.closeAllLoading();
      _loadItems();
      ComponentNotification(
        success: true,
        title: '휴무 취소 성공',
        subTitle: '휴무 취소가 성공하였습니다.',
      ).call();
      setState(() {
        _isViewDetail = false;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '휴무 취소 실패',
        subTitle: '관리자에게 문의하세요.',
      ).call();
    });
  }

  void _showCreateDialog(HolidayCreateRequest holidayCreateRequest) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('${holidayCreateRequest.dateDesired} 휴무 등록'),
            content:
            Text('정말 ${holidayCreateRequest.dateDesired} 휴무를 등록하시겠습니까?'),
            actions: [
              OutlinedButton(
                child: const Text('확인'),
                onPressed: () async {
                  _doSetHoliday(context, holidayCreateRequest);
                  Navigator.of(context).pop();
                },
              ),
              OutlinedButton(
                child: const Text('취소'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  void _showDelDialog(String dateDesired) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('$dateDesired 휴무 취소'),
            content: Text('정말 $dateDesired 휴무를 취소하시겠습니까?'),
            actions: [
              OutlinedButton(
                child: const Text('확인'),
                onPressed: () async {
                  _doDelHoliday(context, dateDesired);
                  Navigator.of(context).pop();
                },
              ),
              OutlinedButton(
                child: const Text('취소'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  void _handleMonthChange(date) {
    setState(() {
      _isViewDetail = true;
    });
  }

  void _handleSelectedDate(date) {
    setState(() {
      _selectedDay = date;
    });
  }

}
