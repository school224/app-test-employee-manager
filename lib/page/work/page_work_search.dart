import 'package:app_test_employee_manager/components/component_appbar_actions.dart';
import 'package:app_test_employee_manager/components/component_count_title.dart';
import 'package:app_test_employee_manager/components/component_custom_loading.dart';
import 'package:app_test_employee_manager/components/component_no_contents.dart';
import 'package:app_test_employee_manager/components/component_notification.dart';
import 'package:app_test_employee_manager/components/component_work_detail_item.dart';
import 'package:app_test_employee_manager/config/config_color.dart';
import 'package:app_test_employee_manager/config/config_size.dart';
import 'package:app_test_employee_manager/model/work/work_detail_item.dart';
import 'package:app_test_employee_manager/repository/repo_work.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageWorkSearch extends StatefulWidget {
  const PageWorkSearch({Key? key}) : super(key: key);

  @override
  State<PageWorkSearch> createState() => _PageWorkSearchState();
}

class _PageWorkSearchState extends State<PageWorkSearch> {
  final _scrollController = ScrollController();

  List<WorkDetailItem> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;

  @override
  void initState() {
    super.initState();
    _getList();
  }

  Future<void> _getList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoWork().getMemberWorkList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      // 이번에 추가된 알림창 컴포넌트. component_notification 참고
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        controller: _scrollController,
        children: [
          Container(
            height: 100,
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              gradient: const LinearGradient(
                begin: Alignment.bottomRight,
                end: Alignment.topLeft,
                colors: [
                  Color(0xff4dabf7),
                  Color(0xffda77f2),
                  Color(0xfff783ac),
                ],
              ),
            ),
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Container(color: Colors.white,
              child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('나의 출근장부 조회하기',
                style: TextStyle(
                  fontSize: fontSizeSuper
                ),),
              ],
            ),),
          ),
          SizedBox(height: 10,),
          ComponentCountTitle(icon: Icons.favorite, count: _totalItemCount, unitName: '건', itemName: '출퇴근 데이터',),
          SizedBox(height: 10,),
          _buildList(),
        ],
      ),
    );
  }


  Widget _buildList() {
    if (_totalItemCount > 0) {
      return Container(
        // decoration: BoxDecoration(
        //   border: Border.all(
        //       width: 5,
        //       color: Colors.brown,
        //   ),
        // ),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) => ComponentWorkDetailItem(
                workDetailItem: _list[index],
                callback: () async {},
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 45,
        child: const ComponentNoContents(icon: Icons.not_interested, msg: '데이터가 없습니다.'),
      );
    }
  }
}
