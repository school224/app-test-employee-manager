import 'package:app_test_employee_manager/components/component_custom_loading.dart';
import 'package:app_test_employee_manager/components/component_notification.dart';
import 'package:app_test_employee_manager/model/work/work_detail_item.dart';
import 'package:app_test_employee_manager/repository/repo_work.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageWorkTest extends StatefulWidget {
  const PageWorkTest({Key? key}) : super(key: key);

  @override
  State<PageWorkTest> createState() => _PageWorkTestState();
}

class _PageWorkTestState extends State<PageWorkTest> {
  List<WorkDetailItem> _list = [];

  Future<void> _getList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoWork().getMemberWorkList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      // 이번에 추가된 알림창 컴포넌트. component_notification 참고
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();
    });
  }

  @override
  void initState() {
    super.initState();
    print(_list);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    );
  }
}
