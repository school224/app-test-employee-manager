import 'package:animated_button/animated_button.dart';
import 'package:app_test_employee_manager/components/component_appbar_actions.dart';
import 'package:app_test_employee_manager/components/component_custom_loading.dart';
import 'package:app_test_employee_manager/components/component_notification.dart';
import 'package:app_test_employee_manager/config/config_size.dart';
import 'package:app_test_employee_manager/repository/repo_work.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageWork extends StatefulWidget {
  const PageWork({super.key});

  @override
  State<PageWork> createState() => _PageWorkState();
}

class _PageWorkState extends State<PageWork> {
  String _workStatusName = '';
  String _workStatus = '';

  @override
  void initState() {
    _getData();
    super.initState();
  }

  Future<void> _getData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoWork().getData().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _workStatusName = res.data!.workStatusName;
        _workStatus = res.data!.workStatus;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(success: false, title: '실패', subTitle: '회원 정보를 가져오는데 실패했습니다.').call();
    });
  }

  Future<void> _putData(String workStatus) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoWork().putStatus(workStatus).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _workStatusName = res.data!.workStatusName;
        _workStatus = res.data!.workStatus;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(success: false, title: '실패', subTitle: '상태를 수정하는데 실패했습니다.').call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Container(
              width: 320,
              height: 120,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(20)),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Colors.green.shade800,
                        Colors.yellow.shade600,
                      ])),
              child: Container(
                width: 300,
                height: 100,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                alignment: Alignment.center,
                child: Text('현재상태 : $_workStatusName',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: fontSizeSuper
                ),),
              ),
            ),
            _buildBody(),
          ],
        ),
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 100,),
          if (_workStatus == 'NO_STATUS')
            AnimatedButton(
              onPressed: () {
                _putData('ATTENDANCE');
              },
              child: Text(
                "출근",
                style: TextStyle(
                    fontSize: fontSizeBig,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              color: Colors.brown,
              shadowDegree: ShadowDegree.light,
              duration: 400,
            ),
          if (_workStatus == 'ATTENDANCE')
            AnimatedButton(
              onPressed: () {
                _putData('EARLY_LEAVE');
              },
              child: Text(
                "조퇴",
                style: TextStyle(
                    fontSize: fontSizeBig,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              color: Colors.brown,
              shadowDegree: ShadowDegree.light,
              duration: 400,
            ),
          if (_workStatus == 'ATTENDANCE')
            AnimatedButton(
              onPressed: () {
                _putData('LEAVE_WORK');
              },
              child: Text(
                "퇴근",
                style: TextStyle(
                    fontSize: fontSizeBig,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              color: Colors.brown,
              shadowDegree: ShadowDegree.light,
              duration: 400,
            ),
          if (_workStatus == 'EARLY_LEAVE' || _workStatus == 'LEAVE_WORK')
            Text('퇴근 후에는 상태를 변경할 수 없습니다.\n오늘도 고생 많으셨어요!',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: fontSizeBig
            ),),
        ],
      ),
    );
  }
}
