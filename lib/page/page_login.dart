import 'package:app_test_employee_manager/components/component_appbar_popup.dart';
import 'package:app_test_employee_manager/components/component_custom_loading.dart';
import 'package:app_test_employee_manager/components/component_notification.dart';
import 'package:app_test_employee_manager/config/config_color.dart';
import 'package:app_test_employee_manager/config/config_form_validator.dart';
import 'package:app_test_employee_manager/functions/token_lib.dart';
import 'package:app_test_employee_manager/middleware/middleware_login_check.dart';
import 'package:app_test_employee_manager/model/member/member_login_request.dart';
import 'package:app_test_employee_manager/repository/repo_member.dart';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:flutter/material.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(MemberLoginRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc)
    {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doLogin(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setToken(res.data);

      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
     MiddlewareLoginCheck().check(context);

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: '로그인',
      ),
      body: Center(
        child: Stack(
          children: [
            _buildBody()
          ],
        ),
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 400,
              child: FormBuilderTextField(
                  cursorColor: Colors.brown,
                  name: 'username',
                  decoration: const InputDecoration(
                    contentPadding: EdgeInsets.all(8),
                    fillColor: Colors.white,
                    filled: true,
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(Radius.circular(5)),
                    ),
                    prefixIcon: Icon(Icons.person),
                    labelText: '아이디',
                    hintText: '아이디를 입력하세요'
                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired),
                    FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)),
                    FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                  ]),
                  keyboardType: TextInputType.text,
                ),
            ),
            SizedBox(height: 30,),
            SizedBox(
              width: 400,
              child: FormBuilderTextField(
                name: 'password',
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.all(8),
                  fillColor: Colors.white,
                  filled: true,
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(Radius.circular(5)),
                  ),
                  prefixIcon: Icon(Icons.key),
                  labelText: '비밀번호',
                  hintText: '비밀번호를 입력하세요',
                ),
                obscureText: true,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(5,
                      errorText: formErrorMinLength(5)),
                  FormBuilderValidators.maxLength(20,
                      errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
            ),
            SizedBox(
              height: 100,
            ),
            SizedBox(
              height: 50,
              width: 100,
              child: ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState?.saveAndValidate() ?? false) {
                    MemberLoginRequest request = MemberLoginRequest(
                      _formKey.currentState!.fields['username']!.value,
                      _formKey.currentState!.fields['password']!.value,
                    );
                    _doLogin(request);
                  }
                },
                child: Text(
                  '로그인',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                style:
                    ElevatedButton.styleFrom(backgroundColor: colorSecondary),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
