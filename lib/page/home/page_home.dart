import 'package:app_test_employee_manager/components/component_custom_loading.dart';
import 'package:app_test_employee_manager/config/config_size.dart';
import 'package:app_test_employee_manager/functions/token_lib.dart';
import 'package:app_test_employee_manager/model/member/member_login_response.dart';
import 'package:app_test_employee_manager/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageHome extends StatefulWidget {
  const PageHome({Key? key}) : super(key: key);

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  String _memberName = '';

  @override
  void initState() {
    _getMemberName();
    super.initState();
  }

  Future<void> _getMemberName() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) => ComponentCustomLoading(cancelFunc: cancelFunc));
    
    await RepoMember().getMemberItem().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _memberName = res.data.memberFullName;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      TokenLib.logout(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/g5.gif',
                  fit: BoxFit.cover,
                ),
              ],
            ),
          ),
          SizedBox(height: 50,),
          Container(
            padding: const EdgeInsets.all(5),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RotatedBox(quarterTurns: 3,
                child: Text('뿌라밤',
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.pink, fontSize: fontSizeSuper),),),
                SizedBox(width: 10,),
                Text(
                  '${_memberName}님\n환영합니다! \n오늘도 좋은 하루 되세요!',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: fontSizeSuper),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
