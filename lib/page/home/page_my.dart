import 'dart:io';

import 'package:app_test_employee_manager/components/component_custom_loading.dart';
import 'package:app_test_employee_manager/components/component_default_button.dart';
import 'package:app_test_employee_manager/components/component_item_property.dart';
import 'package:app_test_employee_manager/components/component_notification.dart';
import 'package:app_test_employee_manager/config/config_color.dart';
import 'package:app_test_employee_manager/config/config_size.dart';
import 'package:app_test_employee_manager/model/member/member_item.dart';
import 'package:app_test_employee_manager/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PageMy extends StatefulWidget {
  const PageMy({Key? key}) : super(key: key);

  @override
  State<PageMy> createState() => _PageMyState();
}

class _PageMyState extends State<PageMy> {

  MemberItem? _memberItem;

  //todo : 회원 성별에 맞춰 프로필 사진 변경기능 넣기
  // String _backgroundImage = '';
  // String _setImage() {
  //   String _gender = "${_memberItem?.gender}";
  //
  //   if(_gender == "MAN") {
  //     _backgroundImage = "assets/02.jpg";
  //   } else {
  //     _backgroundImage = "assets/03.jpg";
  //   }
  //   return _backgroundImage;
  // }

  @override
  void initState() {
    getMemberItem();
    super.initState();
  }

  Future<void> getMemberItem() async {
    BotToast.showCustomLoading(
        toastBuilder: (cancelFunc) => ComponentCustomLoading(cancelFunc: cancelFunc)
    );

    await RepoMember().getMemberItem().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _memberItem = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      Navigator.pop(context);

      ComponentNotification(
          success: false,
          title: '불러오기 실패',
          subTitle: '데이터를 불러오지 못했습니다.').call();
    });
  }

  Future<void> _delData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().delData().then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '데이터 삭제 성공',
        subTitle: res.msg,
      ).call();

      Navigator.pop(context); // 다이알로그 창 닫기
      Navigator.pop(context, [true]); // 상세페이지 닫으면서 부모페이지(리스트페이지)에 새로고침 해야한다고 말해주기
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 삭제 실패',
        subTitle: '데이터 삭제에 실패하였습니다.',
      ).call();

      // 삭제가 실패했으므로 현재창 닫지 않음.
      Navigator.pop(context); // 다이알로그 창 닫기
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            gradient: const LinearGradient(
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
              colors: [
                Color(0xff4dabf7),
                Color(0xffda77f2),
                Color(0xfff783ac),
              ],
            ),
            borderRadius: BorderRadius.circular(200),
          ),
          child: CircleAvatar(
            radius: 80,
            backgroundImage: AssetImage('assets/02.jpg'),
          ),
        ),
        _buildBody(),
        const SizedBox(height: 20,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ComponentDefaultButton(callback: () {_showDeleteDialog();}, name: '퇴사'),
          ],
        ),
      ],
    ));
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Center(
        child:
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ComponentMemberItemProperty(title: '회원', contents: _memberItem?.memberFullName ?? ''),
                ComponentMemberItemProperty(title: '사원 번호', contents: _memberItem?.employeeNumber ?? ''),
                ComponentMemberItemProperty(title: '연락처', contents: _memberItem?.memberPhone ?? ''),
                ComponentMemberItemProperty(title: '생년월일', contents: _memberItem?.birthday ?? ''),
                ComponentMemberItemProperty(title: '성별', contents: _memberItem?.gender ?? ''),
                ComponentMemberItemProperty(title: '입사일', contents: _memberItem?.dateJoin ?? ''),
              ],
            ),
          ),

      ),
    );
  }

  void _showDeleteDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('퇴사하기'),
            content: const Text('정말 퇴사처리 할까요?'),
            actions: [
              OutlinedButton(
                  onPressed: () {
                    _delData();
                  },
                  child: const Text('확인')
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }
}

//todo: androidManifest에 갤러리.인터넷 퍼미션 넣어야한다. - 프로필 사진 이미지 : 갤러리접근
