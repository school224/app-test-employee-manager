import 'package:app_test_employee_manager/config/config_color.dart';
import 'package:app_test_employee_manager/page/holiday/page_holiday_search.dart';
import 'package:app_test_employee_manager/page/holiday/page_holiday.dart';
import 'package:app_test_employee_manager/page/home/page_home.dart';
import 'package:app_test_employee_manager/page/home/page_my.dart';
import 'package:app_test_employee_manager/page/work/page_work.dart';
import 'package:app_test_employee_manager/page/work/page_work_search.dart';
import 'package:app_test_employee_manager/page/work/page_work_test.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);


  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _selectedIndex = 0;

  List<BottomNavigationBarItem> bottomItems = [
    BottomNavigationBarItem(icon: Icon(Icons.home), label: '홈 화면'),
    BottomNavigationBarItem(icon: Icon(Icons.person), label: '출결 체크'),
    BottomNavigationBarItem(icon: Icon(Icons.folder_shared), label: '출결 조회'),
    BottomNavigationBarItem(icon: Icon(Icons.access_time), label: '연차 신청'),
    BottomNavigationBarItem(icon: Icon(Icons.battery_charging_full), label: '연차 내역 조회'),
    BottomNavigationBarItem(icon: Icon(Icons.handshake), label: '마이페이지'),
  ];

  List appTitles=[
    Text('메인 화면'),
    Text('출결 체크'),
    Text('출결 조회'),
    Text('연차 신청'),
    Text('연차 내역 조회'),
    Text('마이 페이지'),
  ];

  List pages=[
    PageHome(),
    PageWork(),
    PageWorkSearch(),
    // PageWorkTest(),
    PageHoliday(),
    PageHolidaySearch(),
    PageMy(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: appTitles[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.brown,
        selectedItemColor: colorPrimary,
        unselectedItemColor: colorSecondary,
        selectedFontSize: 14,
        unselectedFontSize: 10,
        currentIndex: _selectedIndex,

        showUnselectedLabels: false,
        showSelectedLabels: false,
        onTap: (int index) {
          setState(() {
            _selectedIndex = index;
          });
        },
        items: bottomItems,
      ),
      body: pages[_selectedIndex],
    );
  }
}
