import 'package:app_test_employee_manager/components/component_appbar_popup.dart';
import 'package:app_test_employee_manager/components/component_default_button.dart';
import 'package:app_test_employee_manager/components/component_notification.dart';
import 'package:app_test_employee_manager/config/config_form_builder_field_option.dart';
import 'package:app_test_employee_manager/config/config_form_validator.dart';
import 'package:app_test_employee_manager/model/member/member_join_request.dart';
import 'package:app_test_employee_manager/page/page_start.dart';
import 'package:app_test_employee_manager/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageJoin extends StatefulWidget {
  const PageJoin({Key? key}) : super(key: key);

  @override
  State<PageJoin> createState() => _PageJoinState();
}

class _PageJoinState extends State<PageJoin> {
  final _formKey = GlobalKey<FormBuilderState>();
  String _selectedDepartment = 'PERSONNEL';
  String _selectedPosition = 'STAFF';
  String _selectedGender = "MAN";


  Future<void> _memberJoin(MemberJoinRequest joinRequest) async {
    await RepoMember()
        .memberJoin(joinRequest)
        .then((res) {
      BotToast.closeAllLoading();

      if (res.isSuccess) {
        ComponentNotification(
          success: true,
          title: '사원등록 완료',
          subTitle: '사원등록이 완료되었습니다.',
        ).call();

        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => const PageStart()), (route) => false);
      } else {
        ComponentNotification(
          success: true,
          title: '사원등록이 완료되지 않았습니다.',
          subTitle: res.msg,
        ).call();
      }

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '사원등록 실패',
        subTitle: '사원등록이 완료되지 않았습니다.',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: '사원 등록하기'),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: ComponentDefaultButton(
            callback: () {
              if (_formKey.currentState?.saveAndValidate() ?? false) {
                MemberJoinRequest joinRequest = MemberJoinRequest(
                    _formKey.currentState!.fields['employeeNumber']!.value,
                    _formKey.currentState!.fields['name']!.value,
                    _formKey.currentState!.fields['phone']!.value,
                    DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['birthday']!.value),
                    _formKey.currentState!.fields['gender']!.value, //라디오
                    _formKey.currentState!.fields['department']!.value, //라디오
                    _formKey.currentState!.fields['position']!.value, //라디오
                    _formKey.currentState!.fields['username']!.value,
                    _formKey.currentState!.fields['password']!.value,

                );

                _memberJoin(joinRequest);
              }
            },
            name: '등록'),
      ),

    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  FormBuilderTextField(
                    cursorColor: Colors.brown,
                    name: 'username',
                    decoration: const InputDecoration(
                        contentPadding: EdgeInsets.all(8),
                        fillColor: Colors.white,
                        filled: true,
                        border: OutlineInputBorder(
                          borderRadius: const BorderRadius.all(Radius.circular(5)),
                        ),
                        prefixIcon: Icon(Icons.person),
                        labelText: '아이디',
                        hintText: '아이디(6-20자)를 입력하세요'
                    ),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(6, errorText: formErrorMinLength(6)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(height: 10,),
                  FormBuilderTextField(
                    name: 'password',
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.all(8),
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                      ),
                      prefixIcon: Icon(Icons.key),
                      labelText: '비밀번호',
                      hintText: '비밀번호(8-20자)를 입력하세요',
                    ),
                    obscureText: true,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8,
                          errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20,
                          errorText: formErrorMaxLength(20)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(height: 10,),
                  FormBuilderTextField(
                    name: 'employeeNumber',
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.all(8),
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                      ),
                      prefixIcon: Icon(Icons.numbers),
                      labelText: '사원번호',
                      hintText: '사원번호(10자)를 입력하세요',
                    ),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(10,
                          errorText: formErrorMinLength(10)),
                      FormBuilderValidators.maxLength(10,
                          errorText: formErrorMaxLength(10)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(height: 10,),
                  FormBuilderRadioGroup<String>(
                    orientation: OptionsOrientation.vertical,
                    wrapVerticalDirection: VerticalDirection.down,
                    decoration: const InputDecoration(
                      labelText: '부서',
                    ),
                    initialValue: _selectedDepartment,
                    onChanged: (val) {
                      setState(() {
                        _selectedDepartment = val!;
                      });
                    },
                    name: 'department',
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(errorText: formErrorRequired)]),
                    options: radioDepartment,
                    controlAffinity: ControlAffinity.trailing,
                  ),
                  Text('현재 선택된 부서 : $_selectedDepartment'),
                  const SizedBox(height: 10,),
                  FormBuilderRadioGroup<String>(
                    decoration: const InputDecoration(
                      labelText: '직급',
                    ),
                    initialValue: _selectedPosition,
                    onChanged: (val) {
                      setState(() {
                        _selectedPosition = val!;
                      });
                    },
                    name: 'position',
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(errorText: formErrorRequired)]),
                    options: radioPosition,
                    controlAffinity: ControlAffinity.trailing,
                  ),
                  Text('현재 선택된 직급 : $_selectedPosition'),
                  const SizedBox(height: 10,),
                  FormBuilderTextField(
                    name: 'name',
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.all(8),
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                      ),
                      prefixIcon: Icon(Icons.person),
                      labelText: '이름',
                      hintText: '이름(2-20자)를 입력하세요',
                    ),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20,
                          errorText: formErrorMaxLength(20)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(height: 10,),
                  FormBuilderTextField(
                    name: 'phone',
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.all(8),
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                      ),
                      prefixIcon: Icon(Icons.phone_android),
                      labelText: '핸드폰 번호',
                      hintText: '핸드폰 번호(10-15자)를 입력하세요',
                    ),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(10,
                          errorText: formErrorMinLength(10)),
                      FormBuilderValidators.maxLength(15,
                          errorText: formErrorMaxLength(15)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(height: 10,),
                  FormBuilderDateTimePicker(
                    name: 'birthday',
                    format: DateFormat('yyyy-MM-dd'),
                    inputType: InputType.date,
                    decoration: InputDecoration(
                      labelText: '생년월일',
                      suffixIcon:IconButton(
                        icon: const Icon(Icons.calendar_month),
                        onPressed: () {
                          _formKey.currentState!.fields['birthday']
                              ?.didChange(null);
                        },
                      ),
                    ),
                    locale: const Locale.fromSubtags(languageCode: 'ko'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired)
                    ]),
                  ),
                  const SizedBox(height: 10,),
                  FormBuilderRadioGroup<String>(
                    decoration: const InputDecoration(
                      labelText: '성별',
                    ),
                    initialValue: _selectedGender,
                    onChanged: (val) {
                      setState(() {
                        _selectedGender = val!;
                      });
                    },
                    name: 'gender',
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(errorText: formErrorRequired)]),
                    options: radioGender,
                    controlAffinity: ControlAffinity.trailing,
                  ),
                  Text('현재 선택된 성별 : $_selectedGender'),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
