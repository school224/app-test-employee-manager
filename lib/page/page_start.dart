import 'package:app_test_employee_manager/components/component_appbar_actions.dart';
import 'package:app_test_employee_manager/components/component_default_button.dart';
import 'package:app_test_employee_manager/config/config_color.dart';
import 'package:app_test_employee_manager/page/page_join.dart';
import 'package:app_test_employee_manager/page/page_login.dart';
import 'package:flutter/material.dart';

class PageStart extends StatelessWidget {
  const PageStart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '사원관리 APP 메인',
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 100,
            width: 300,
            child: Icon(Icons.key_off,
            size: 100,
            color: colorSecondary,),
          ),
          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(50),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border.all(
                  width: 3,
                  color: Colors.black
              ),
            ),
            child: Text(
              '접근권한이 없습니다. \n 로그인해주세요.',
              style: TextStyle(fontSize: 20),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ComponentDefaultButton(
                  callback: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => PageJoin()));
                  },
                  name: '등록'
              ),
              SizedBox(width: 30,),
              ComponentDefaultButton(
                  callback: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => PageLogin()));
                  },
                  name: '로그인'
              ),
            ],
          ),
        ],
      ),
    );
  }
}