import 'package:app_test_employee_manager/components/component_notification.dart';
import 'package:app_test_employee_manager/model/member/member_login_response.dart';
import 'package:app_test_employee_manager/model/work/work_response.dart';
import 'package:app_test_employee_manager/page/page_index.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

/*
SharedPreferences 에 데이터가 저장되는 방식은 Map임을 반드시 숙지하고 갈 것.
Map이 무엇인지 알아야 함.
 */
class TokenLib {

  // token 세팅
  static void setToken(MemberLoginResponse response) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberId', response.memberId.toString());
    prefs.setString('username', response.username.toString());
    prefs.setString('dateJoin', response.dateJoin.toString());
  }

  // token 가져오기
  static Future<MemberLoginResponse?> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? memberId = prefs.getString('memberId');
    String? username = prefs.getString('username');
    String? dateJoin = prefs.getString('dateJoin');

    if (memberId == null || username == null || dateJoin == null) {
      return null;
    } else {
      return MemberLoginResponse(int.parse(memberId), username, dateJoin);
    }
  }

  // 로그아웃 처리
  static void logout(BuildContext context) async {
    // 기존에 닫혀있던 토스트팝업, 커스텀로딩 다 닫고
    BotToast.closeAllLoading();

    // 메모리에 접근해서 key 가 token인 값을 빈문자열('')로 바꿔버리고
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberId', '0');
    prefs.setString('username', '');
    prefs.setString('dateJoin', '');

    // 토스트메시지 띄우고
    ComponentNotification(
      success: false,
      title: '로그아웃',
      subTitle: '로그아웃되어 로그인 화면으로 이동합니다.',
    ).call();

    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageIndex()), (route) => false);
  }
}
