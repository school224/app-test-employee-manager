import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

const List<FormBuilderFieldOption<String>> radioGender = [
  FormBuilderFieldOption(value: 'MAN', child: Text('남자'),),
  FormBuilderFieldOption(value: 'WOMAN', child: Text('여자'),),
];

const List<FormBuilderFieldOption<String>> radioDepartment = [
  FormBuilderFieldOption(value: 'PERSONNEL', child: Text('인사부'),),
  FormBuilderFieldOption(value: 'PLANNING', child: Text('기획부'),),
  FormBuilderFieldOption(value: 'SALES', child: Text('영업부'),),
  FormBuilderFieldOption(value: 'ACCOUNTING', child: Text('경리부'),),
];

const List<FormBuilderFieldOption<String>> radioPosition = [
  FormBuilderFieldOption(value: 'STAFF', child: Text('사원'),),
  FormBuilderFieldOption(value: 'DEPUTY', child: Text('대리'),),
  FormBuilderFieldOption(value: 'SECTION_CHIEF', child: Text('과장'),),
  FormBuilderFieldOption(value: 'SECTION_HEAD', child: Text('부장'),),
];
