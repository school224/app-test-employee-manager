import 'package:flutter/material.dart';

const List<DropdownMenuItem<String>> dropdownMachineType = [
  DropdownMenuItem(value: 'WASHER', child: Text('세탁기')),
  DropdownMenuItem(value: 'DRYER', child: Text('건조기')),
];
