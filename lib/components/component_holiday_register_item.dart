import 'package:app_test_employee_manager/config/config_color.dart';
import 'package:app_test_employee_manager/model/holiday/holiday_register_item.dart';
import 'package:flutter/material.dart';

class ComponentHolidayRegisterItem extends StatelessWidget {
  const ComponentHolidayRegisterItem({super.key, required this.holidayRegisterItem, required this.callback});

  final HolidayRegisterItem holidayRegisterItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            topRight: Radius.circular(25),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10),
          ),
        ),
          elevation: 4.0,
          child: ListTile(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
            ),
            tileColor: Colors.brown,
            textColor: Colors.white,
            iconColor: colorPrimary,
            leading: const Icon(Icons.volunteer_activism),
            title: Text('사원 :${holidayRegisterItem.memberFullName}'),
            subtitle: Text('연차 신청일자 : ${holidayRegisterItem.dateDesired}\n연차 사유 : ${holidayRegisterItem.reason}\n연차 종류 : ${holidayRegisterItem.holidayType}'),
          ),
      ),
    );
  }
}


