import 'package:app_test_employee_manager/config/config_color.dart';
import 'package:app_test_employee_manager/model/work/work_detail_item.dart';
import 'package:flutter/material.dart';

class ComponentWorkDetailItem extends StatelessWidget {
  const ComponentWorkDetailItem({super.key, required this.workDetailItem, required this.callback});

  final WorkDetailItem workDetailItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Card(
        shape: RoundedRectangleBorder(
          side: const BorderSide(color: Colors.brown, width: 4),
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 3.0,
        child: ListTile(
          tileColor: Colors.white,
          shape: RoundedRectangleBorder(
            side: const BorderSide(color: Colors.brown, width: 4),
            borderRadius: BorderRadius.circular(15),
          ),
          iconColor: Colors.brown,
          leading: const Icon(Icons.contact_mail),
          title: Text('사원 : ${workDetailItem.memberFullName}'),
          subtitle:
          Text('출근일 : ${workDetailItem.dateWork}\n출근시간 : ${workDetailItem.inWork}, 퇴근시간 : ${workDetailItem.outWork} '),
        ),
      ),
    );
  }
}




