import 'package:app_test_employee_manager/config/config_color.dart';
import 'package:app_test_employee_manager/config/config_size.dart';
import 'package:flutter/material.dart';

class ComponentDefaultButton extends StatefulWidget {
  const ComponentDefaultButton({super.key, required this.callback, required this.name});

  final VoidCallback callback;
  final String name;

  @override
  State<ComponentDefaultButton> createState() => _ComponentDefaultButtonState();
}

class _ComponentDefaultButtonState extends State<ComponentDefaultButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: 100,
      child: ElevatedButton(
        onPressed: (widget.callback),
        child: Text(
          widget.name,
          style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: fontSizeBig),
        ),
        style: ElevatedButton.styleFrom(backgroundColor: colorSecondary),
      ),
    );
  }
}
