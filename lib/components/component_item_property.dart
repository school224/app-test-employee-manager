import 'package:app_test_employee_manager/config/config_size.dart';
import 'package:flutter/material.dart';

class ComponentMemberItemProperty extends StatelessWidget {
  const ComponentMemberItemProperty({super.key, required this.title, required this.contents});

  final String title;
  final String contents;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(5),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 10,),
          Text(title,
          style: const TextStyle(
            fontSize: fontSizeSm,
            fontWeight: FontWeight.bold
          ),),
          const SizedBox(height: 10,),
          Text(contents,
          style: const TextStyle(
            fontSize: fontSizeMicro,
          ),),
          const SizedBox(height: 10,)
        ],
      ),
    );
  }
}
