import 'package:app_test_employee_manager/model/work/work_detail_item.dart';

class WorkDetailResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;
  List<WorkDetailItem> list;

  WorkDetailResult(this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg, this.list);

  factory WorkDetailResult.fromJson(Map<String, dynamic> json) {
    return WorkDetailResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => WorkDetailItem.fromJson(e)).toList(),
      // (json['list'] as List).map((e) => WorkDetailItem.fromJson(e)).toList(),
      // json['list'] == null ? [] : (json['list'] as List).map((e) => WorkDetailItem.fromJson(e)).toList()
      // data: json['data'] ? WorkResponse.fromJson(json['data']) : null,
    );
  }
}