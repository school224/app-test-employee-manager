class WorkResponse {
  String workStatusName;
  String workStatus;

  WorkResponse(this.workStatus, this.workStatusName);

  factory WorkResponse.fromJson(Map<String, dynamic> json) {
    return WorkResponse(
      json['workStatus'],
      json['workStatusName'],
    );
  }
}