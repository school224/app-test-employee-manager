class WorkDetailItem {
  int? workId;
  int? memberId;
  String? memberFullName;
  String? dateWork;
  String? workStatus;
  String? inWork;
  String? pauseWork;
  String? returnWork;
  String? outWork;

  WorkDetailItem({this.workId, this.memberId, this.memberFullName, this.dateWork, this.workStatus, this.inWork, this.pauseWork, this.returnWork, this.outWork});

  factory WorkDetailItem.fromJson(Map<String, dynamic> json) {
    return WorkDetailItem(
      workId: json['workId'] != null ? json['workId'] : null,
      memberId: json['memberId'] != null ? json['memberId'] : null,
      memberFullName: json['memberFullName'] != null ? json['memberFullName'] : null,
      dateWork: json['dateWork'] != null ? json['dateWork'] : null,
      workStatus: json['workStatus'] != null ? json['workStatus'] : null,
      inWork: json['inWork'] != null ? json['inWork'] : null,
      pauseWork: json['pauseWork'] != null ? json['pauseWork'] : null,
      returnWork: json['returnWork'] != null ? json['returnWork'] : null,
      outWork: json['outWork'] != null ? json['outWork'] : null,
      // int.parse(json['workId']),
      // int.parse(json['memberId']),
      // json['memberFullName'],
      // DateTime.parse(json['dateWork']),
      // json['workStatus'],
      // DateTime.parse(json['inWork']),
      // json['pauseWork'] == null ? null : DateTime.parse('${json['pauseWork']}'),
      // json['returnWork'] == null ? null : DateTime.parse('${json['returnWork']}'),
      // json['outWork'] == null ? null : DateTime.parse('${json['outWork']}'),
    );
  }
}