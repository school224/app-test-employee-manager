import 'package:app_test_employee_manager/model/work/work_response.dart';

class WorkResult {
  bool isSuccess;
  int code;
  String msg;
  WorkResponse? data;

  WorkResult(this.isSuccess, this.code, this.msg, this.data);

  factory WorkResult.fromJson(Map<String, dynamic> json) {
    return WorkResult(
        json['isSuccess'] as bool,
        json['code'],
        json['msg'],
        WorkResponse.fromJson(json['data']),
        // data: json['data'] ? WorkResponse.fromJson(json['data']) : null,
    );
  }
}