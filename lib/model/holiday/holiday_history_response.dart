import 'package:app_test_employee_manager/model/holiday/holiday_history_item.dart';

class HolidayHistoryResponse {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<HolidayHistoryItem>? list;

  HolidayHistoryResponse(
      this.totalItemCount,
      this.totalPage,
      this.currentPage, {
        this.list,
      });

  factory HolidayHistoryResponse.fromJson(Map<String, dynamic> json) {
    return HolidayHistoryResponse(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null
          ? (json['list'] as List)
          .map((e) => HolidayHistoryItem.fromJson(e))
          .toList()
          : null,
    );
  }

}