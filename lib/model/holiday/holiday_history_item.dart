class HolidayHistoryItem {
  int memberId;
  String memberFullName;
  DateTime dateDesired;
  String holidayType;
  String reason;


  HolidayHistoryItem(
      this.memberId,
      this.memberFullName,
      this.dateDesired,
      this.holidayType,
      this.reason,
      );

  factory HolidayHistoryItem.fromJson(Map<String, dynamic> json) {
    return HolidayHistoryItem(
      json['memberId'],
      json['memberFullName'],
      DateTime.parse(json['dateDesired']),
      json['holidayType'],
      json['reason'],
    );
  }

}