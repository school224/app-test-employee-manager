class HolidayCreateRequest {
  String holidayType;
  String dateDesired;
  String reason;

  HolidayCreateRequest(this.holidayType, this.dateDesired, this.reason);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['holidayType'] = this.holidayType;
    data['dateHoliday'] = this.dateDesired;
    data['reason'] = this.reason;

    return data;
  }

}