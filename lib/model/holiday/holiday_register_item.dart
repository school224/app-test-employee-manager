class HolidayRegisterItem {
   int? holidayHistoryId; //휴가신청 시퀀스
   int? memberId; //사원 시퀀스
   String? memberFullName; // [부서] 사원 이름 + 사원 직급
   String? holidayType; //휴가 종류 - 연차, 병가 ...
   String? reason; //휴가 사유
   String? dateDesired; //휴가 희망 일자
   String? dateApplication; //휴가 신청 일자
   String? holidayStatus; //승인 상태
   String? dateApproval; //승인 시간

   HolidayRegisterItem({this.holidayHistoryId, this.memberId, this.memberFullName, this.holidayType, this.reason, this.dateDesired, this.dateApplication, this.holidayStatus, this.dateApproval});

   factory HolidayRegisterItem.fromJson(Map<String, dynamic> json) {
     return HolidayRegisterItem(
       holidayHistoryId: json['holidayHistoryId'] != null ? json['holidayHistoryId'] : null,
       memberId: json['memberId'] != null ? json['memberId'] : null,
       memberFullName: json['memberFullName'] != null ? json['memberFullName'] : null,
       holidayType: json['holidayType'] != null ? json['holidayType'] : null,
       reason: json['reason'] != null ? json['reason'] : null,
       dateDesired: json['dateDesired'] != null ? json['dateDesired'] : null,
       dateApplication: json['dateApplication'] != null ? json['dateApplication'] : null,
       holidayStatus: json['holidayStatus'] != null ? json['holidayStatus'] : null,
       dateApproval: json['dateApproval'] != null ? json['dateApproval'] : null,
     );
   }
}