import 'package:app_test_employee_manager/model/holiday/holiday_register_item.dart';

class HolidayRegisterResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;
  List<HolidayRegisterItem> list;

  HolidayRegisterResult(this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg, this.list);

  factory HolidayRegisterResult.fromJson(Map<String, dynamic> json) {
    return HolidayRegisterResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => HolidayRegisterItem.fromJson(e)).toList(),
    );
  }
}