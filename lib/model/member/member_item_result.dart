import 'package:app_test_employee_manager/model/member/member_item.dart';

class MemberItemResult {
  MemberItem data;
  bool isSuccess;
  int code;
  String msg;

  MemberItemResult(this.data, this.isSuccess, this.code, this.msg);

  factory MemberItemResult.fromJson(Map<String, dynamic> json) {
    return MemberItemResult(
      MemberItem.fromJson(json['data']),
      json['isSuccess'],
      json['code'],
      json['msg'],
    );
  }

}