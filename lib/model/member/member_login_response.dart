class MemberLoginResponse {
  int memberId;
  String username;
  String dateJoin;

  MemberLoginResponse(this.memberId, this.username, this.dateJoin);

  factory MemberLoginResponse.fromJson(Map<String, dynamic> json) {
    return MemberLoginResponse(
      json['memberId'],
      json['username'],
      json['dateJoin']
    );
  }
}