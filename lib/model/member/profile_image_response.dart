class ProfileImageResponse {
  String imageName;
  String dateUpload;

  ProfileImageResponse(this.imageName, this.dateUpload);

  factory ProfileImageResponse.fromJson(Map<String, dynamic> json) {
    return ProfileImageResponse(
        json['imageName'],
        json['dateUpload']
    );
  }
}
