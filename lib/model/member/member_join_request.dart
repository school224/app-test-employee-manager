class MemberJoinRequest {
  String employeeNumber;
  String name;
  String phone;
  String birthday;
  String gender;
  String department;
  String position;
  // String? profileImageUrl; //가입시 생략가능한데, 가입할때 비워져있으면 기본값 이미지를 부여해야할것같음.
  String username;
  String password;

  MemberJoinRequest(
      this.employeeNumber, this.name, this.phone,
      this.birthday, this.gender, this.department,
      this.position, this.username, this.password,
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['employeeNumber'] = this.employeeNumber;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['birthday'] = this.birthday;
    data['gender'] = this.gender;
    data['department'] = this.department;
    data['position'] = this.position;
    // profileImageUrl: data['profileImageUrl'] != null ? data['profileImageUrl'] : null;
    data['username'] = this.username;
    data['password'] = this.password;

    print(data);

    return data;
  }
}