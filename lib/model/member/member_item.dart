class MemberItem {
  int memberId;
  String employeeNumber;
  String isManager;
  String memberFullName;
  String memberPhone;
  String birthday;
  String gender;
  String profileImageUrl;
  String isWorking;
  String dateJoin;
  String dateRetire;

  MemberItem(
      this.memberId,
      this.employeeNumber,
      this.isManager,
      this.memberFullName,
      this.memberPhone,
      this.birthday,
      this.gender,
      this.profileImageUrl,
      this.isWorking,
      this.dateJoin,
      this.dateRetire
      );

  factory MemberItem.fromJson(Map<String, dynamic> json) {
    return MemberItem(
      json['memberId'],
      json['employeeNumber'],
      json['isManager'],
      json['memberFullName'],
      json['memberPhone'],
      json['birthday'],
      json['gender'],
      json['profileImageUrl'] ?? '',
      json['isWorking'],
      json['dateJoin'],
      json['dateRetire'] ?? '',
    );
  }
}