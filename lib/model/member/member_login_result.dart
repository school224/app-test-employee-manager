import 'package:app_test_employee_manager/model/member/member_login_response.dart';

class MemberLoginResult {
  MemberLoginResponse data;
  bool isSuccess;
  int code;
  String msg;

  MemberLoginResult(this.data, this.isSuccess, this.code, this.msg);

  factory MemberLoginResult.fromJson(Map<String, dynamic> json) {
    return MemberLoginResult(
        MemberLoginResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']
    );
  }
}