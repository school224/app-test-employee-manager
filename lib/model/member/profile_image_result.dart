import 'package:app_test_employee_manager/model/member/profile_image_response.dart';

class ProfileImageResult {
  bool isSuccess;
  int code;
  String msg;
  ProfileImageResponse? data;

  ProfileImageResult(this.isSuccess, this.code, this.msg, {this.data});

  factory ProfileImageResult.fromJson(Map<String, dynamic> json) {
    return ProfileImageResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      data: json['data'] == null
          ? null
          : ProfileImageResponse.fromJson(json['data']),
    );
  }
}
