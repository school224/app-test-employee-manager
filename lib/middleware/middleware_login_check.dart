
import 'package:app_test_employee_manager/functions/token_lib.dart';
import 'package:app_test_employee_manager/model/member/member_login_response.dart';
import 'package:app_test_employee_manager/page/page_index.dart';
import 'package:app_test_employee_manager/page/page_start.dart';
import 'package:flutter/material.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    MemberLoginResponse? token = await TokenLib.getToken();


    /*
    최초에 앱을 실행하면 token은 null이고
    한번이라도 로그아웃을 했다면 빈문자열임. (로그아웃 시 setToken에서 ''로 삭제해버림)
    token이 없으면 비회원용 메인으로 강제이동
    token이 있으면 회원용 메인으로 강제이동
     */

    // if (token == null || token == ''){
    //   Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageStart()), (route) => false);
    // } else {
    //   Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => PageIndex()), (route) => false);
    // }

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) {
          if (token == null || token.memberId == 0 || token.username == '' || token.dateJoin == '') {
            return const PageStart();
          } else {
            return const PageIndex();
          }
        }),
        (route) => false);
  }
}