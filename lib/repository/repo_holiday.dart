import 'package:app_test_employee_manager/config/config_api.dart';
import 'package:app_test_employee_manager/functions/token_lib.dart';
import 'package:app_test_employee_manager/model/common_result.dart';
import 'package:app_test_employee_manager/model/holiday/holiday_create_request.dart';
import 'package:app_test_employee_manager/model/holiday/holiday_history_response.dart';
import 'package:app_test_employee_manager/model/holiday/holiday_register_result.dart';
import 'package:app_test_employee_manager/model/member/member_login_response.dart';
import 'package:dio/dio.dart';

class RepoHoliday {
  Future<CommonResult> setData(HolidayCreateRequest holidayCreateRequest) async {
    String baseUrl = '$apiUri/holiday/new/my/{memberId}';

    Dio dio = Dio();

    MemberLoginResponse? token = await TokenLib.getToken();
    // 로그인 후 메모리에 저장해두었던 토큰 가져다가 쓸 때.
    final response = await dio.post(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
        data: holidayCreateRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              if (status == 200) {
                return true;
              } else {
                return false;
              }
            }));

    return CommonResult.fromJson(response.data);
  }

  Future<HolidayHistoryResponse> getList() async {
    String baseUrl = '$apiUri/holiday/member/search/{memberId}';

    Dio dio = Dio();

    MemberLoginResponse? token = await TokenLib.getToken();
    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              if (status == 200) {
                return true;
              } else {
                return false;
              }
            }));

    return HolidayHistoryResponse.fromJson(response.data);
  }

  // Future<HolidayHistoryResponse> getListAll() async {
  //   String baseUrl = '/v1/holiday/member/search/all';
  //
  //   Dio dio = Dio();
  //
  //   final response = await dio.get(baseUrl,
  //       options: Options(
  //           followRedirects: false,
  //           validateStatus: (status) {
  //             if (status == 200) {
  //               return true;
  //             } else {
  //               return false;
  //             }
  //           }));
  //
  //   return HolidayHistoryResponse.fromJson(response.data);
  // }

  Future<CommonResult> delData(String dateHoliday) async {
    String baseUrl = '$apiUri/member/holiday-my/date/{dateHoliday}';

    Dio dio = Dio();

    final response =
    await dio.delete(baseUrl.replaceAll('{dateHoliday}', dateHoliday),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              if (status == 200) {
                return true;
              } else {
                return false;
              }
            }));

    return CommonResult.fromJson(response.data);
  }

  Future<HolidayRegisterResult> getAllList() async {
    String baseUrl = '$apiUri/holiday/search';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)
        )
    );
    return HolidayRegisterResult.fromJson(response.data);
  }
}

