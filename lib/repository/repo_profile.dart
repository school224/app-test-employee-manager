import 'dart:io';

import 'package:app_test_employee_manager/config/config_api.dart';
import 'package:app_test_employee_manager/model/common_result.dart';
import 'package:app_test_employee_manager/model/member/profile_image_result.dart';
import 'package:dio/dio.dart';

class RepoProfile {
  Future<ProfileImageResult> getImage() async {
    String baseUrl = '$apiUri/profile/image/member-id/{memberId}';

    Dio dio = Dio();

    try {
      final response = await dio.get(
        baseUrl.replaceAll('{memberId}', '1'),
      );

      return ProfileImageResult.fromJson(response.data);
    } on DioError catch (e) {
      return ProfileImageResult.fromJson(e.response!.data);
    }
  }

  Future<CommonResult> doUpload(File file) async {
    String baseUrl = '$apiUri/profile/image/member-id/{memberId}';

    Dio dio = Dio();

    try {
      FormData formData = FormData.fromMap({
        'file': MultipartFile.fromFile(file.path)
      });

      final response = await dio.post(
        baseUrl.replaceAll('{memberId}', '1'),
        data: formData,
      );

      return CommonResult.fromJson(response.data);
    } on DioError catch (e) {
      return CommonResult.fromJson(e.response!.data);
    }
  }
}
