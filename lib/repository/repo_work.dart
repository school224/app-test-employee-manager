import 'package:app_test_employee_manager/config/config_api.dart';
import 'package:app_test_employee_manager/model/member/member_login_response.dart';
import 'package:app_test_employee_manager/model/work/work_detail_result.dart';
import 'package:app_test_employee_manager/model/work/work_response.dart';
import 'package:app_test_employee_manager/model/work/work_result.dart';
import 'package:app_test_employee_manager/functions/token_lib.dart';
import 'package:dio/dio.dart';

class RepoWork {
  Future<WorkResult> getData() async {
    const String baseUrl = '$apiUri/work/status/member-id/{memberId}';

    Dio dio = Dio();

    MemberLoginResponse? token = (await TokenLib.getToken()) as MemberLoginResponse?;

    final response = await dio.get(
      baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
      options: Options(
        followRedirects: false,
      )
    );

    return WorkResult.fromJson(response.data);
  }

  Future<WorkResult> putStatus(String workStatus) async {
    const String baseUrl = '$apiUri/work/status/{workStatus}/member-id/{memberId}';

    Dio dio = Dio();

    MemberLoginResponse? token = (await TokenLib.getToken()) as MemberLoginResponse?;

    final response = await dio.put(
      baseUrl.replaceAll('{workStatus}', workStatus).replaceAll('{memberId}', token!.memberId.toString()),
      options: Options(
        followRedirects: false,
      )
    );

    return WorkResult.fromJson(response.data);
  }

  Future<WorkDetailResult> getListAll() async {
    String baseUrl = '/v1/work/member/search/all';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));

    return WorkDetailResult.fromJson(response.data);
  }

  Future<WorkDetailResult> getMemberWorkList() async {
    const String baseUrl = '$apiUri/work/member/search/{memberId}';

    Dio dio = Dio();

    MemberLoginResponse? token = await TokenLib.getToken();

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)
        )
    );
    return WorkDetailResult.fromJson(response.data);
  }


// Future<WorkResult> getMyStatus() async {
  //   const String baseUrl = '$apiUri/work/my/status/{memberId}';
  //
  //   Dio dio = Dio();
  //
  //   MemberLoginResponse? token = await TokenLib.getToken();
  //
  //   final response = await dio.get(
  //       baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
  //       options: Options(
  //         followRedirects: false,
  //       )
  //   );
  //
  //   return WorkResult.fromJson(response.data);
  // }
  //
  // Future<WorkResult> putMyStatus(String workStatus) async {
  //   const String baseUrl = '$apiUri/my/{memberId}/status/{workStatus}';
  //
  //   Dio dio = Dio();
  //
  //   MemberLoginResponse? token = await TokenLib.getToken();
  //   WorkResponse? workToken = await TokenLib.getWorkToken();
  //
  //
  //   final response = await dio.put(
  //       baseUrl.replaceAll('{memberId}', token!.memberId.toString()).replaceAll('{workStatus}', workToken!.workStatus.toString()),
  //       options: Options(
  //         followRedirects: false,
  //       )
  //   );
  //
  //   return WorkResult.fromJson(response.data);
  // }
}