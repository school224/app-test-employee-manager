import 'package:app_test_employee_manager/config/config_api.dart';
import 'package:app_test_employee_manager/functions/token_lib.dart';
import 'package:app_test_employee_manager/model/common_result.dart';
import 'package:app_test_employee_manager/model/member/member_item_result.dart';
import 'package:app_test_employee_manager/model/member/member_join_request.dart';
import 'package:app_test_employee_manager/model/member/member_login_request.dart';
import 'package:app_test_employee_manager/model/member/member_login_response.dart';
import 'package:app_test_employee_manager/model/member/member_login_result.dart';
import 'package:dio/dio.dart';

class RepoMember {
  Future<MemberLoginResult> doLogin(MemberLoginRequest request) async {
    const String baseUrl = '$apiUri/login/member';
    Dio dio = Dio();

    final response = await dio.post(
      baseUrl,
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    return MemberLoginResult.fromJson(response.data);
  }

  Future<MemberItemResult> getMemberItem() async {
    const String baseUrl = '$apiUri/member/get/list/{memberId}';

    Dio dio = Dio();

    MemberLoginResponse? token = await TokenLib.getToken();

    final response = await dio.get(
      baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) => (status == 200)
      )
    );
    return MemberItemResult.fromJson(response.data);
  }

  Future<CommonResult> delData() async {
    const String baseUrl = '$apiUri/member/{id}';

    Dio dio = Dio();

    MemberLoginResponse? token = await TokenLib.getToken();

    final response = await dio.delete(
        baseUrl.replaceAll('{id}', token!.memberId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> memberJoin (MemberJoinRequest joinRequest) async {
    const String baseUrl = '$apiUri/member/new';

    Dio dio = Dio();

    MemberLoginResponse? token = await TokenLib.getToken();

    // try {
      final response = await dio.post(
        baseUrl,
        data: joinRequest.toJson(),
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            if (status == 200) return true;
            else {
              print(status);
              return false;
            }
          }
        ),
      );

      return CommonResult.fromJson(response.data);
    // } on DioError catch (e) {
    //   return CommonResult.fromJson(e.response!.data);
    // }
  }

}